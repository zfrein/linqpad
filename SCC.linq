<Query Kind="Program" />

void Main()
{

//	var a = File.ReadAllLines(@"C:\Users\sovchinnikov\Documents\LINQPad Queries\SCC.txt").ToArray();
//
//	var list=new List<List<int>>();
//
//	
//	foreach(string c in a)
//	{
//			var d= Regex.Split(c, @"\D+").Where(s => !string.IsNullOrWhiteSpace(s)).Select(Int32.Parse).ToList();
//   			list.Add(d);	
//	}
	
//	
//	var Graff = new Hashtable();
//	 foreach (var l in list)
//	{
//		if(!Graff.ContainsKey(l[0]))
//			Graff.Add(l[0],new Vertex { Index= l[0]});
//		
//		if(!Graff.ContainsKey(l[1]))
//			Graff.Add(l[1],new Vertex { Index= l[1]});
//		
//		((Vertex)Graff[l[0]]).List.Add(l[1]);
//	}
////	
//	DFS(Graff);
	
	var Graff = new Hashtable();
	
	Graff.Add(1, new Vertex{Index=1, List=new List<int>{2,4}});
	Graff.Add(2, new Vertex{Index=2, List=new List<int>{5}});
	Graff.Add(3, new Vertex{Index=3, List=new List<int>{5,6}});
	Graff.Add(4, new Vertex{Index=4, List=new List<int>{2}});
	Graff.Add(5, new Vertex{Index=5, List=new List<int>{4}});
	Graff.Add(6, new Vertex{Index=6, List=new List<int>{6}});
	
	DFSIterative(Graff);
	
	Graff.Dump();
}


public class Vertex
{
	public Vertex()
	{
		List=new List<int>();
	}
	
	public int Index{get;set; }
	public Color Color { get; set; }
	
	public Vertex Parent { get; set; }
	
	public int Start { get; set; }
	
	public int Finish { get; set; }
	
	public List<int> List {get; set; }
}

public enum Color
{
	White,Gray, Black
}

public int Time = 0;

public void DFS(Hashtable ht)
{
	foreach(Vertex v in ht.Values)
	{
		v.Color=Color.White;
		v.Parent=null;
	}
	Time=0;
	
	foreach (Vertex v in ht.Values)
	{
		if (v.Color==Color.White)
			DFSVisit(ht, v.Index);
	}
}

public void DFSVisit(Hashtable ht, int s)
{
	Time++;
	Vertex u = (Vertex)ht[s];
	u.Start = Time;
	u.Color = Color.Gray;
	
	foreach(int i in u.List)
	{
		Vertex v = (Vertex)ht[i];
		if (v.Color == Color.White)
		{
			v.Parent = u;
			DFSVisit(ht, v.Index);
		}
	}
	u.Color=Color.Black;
	Time++;
	u.Finish=Time;
}


public void DFSIterative(Hashtable g) 
{
	foreach(Vertex q in g.Values)
	{
		var u=q;
		if(u.Color==Color.White)
		{
			u.Color=Color.Gray;
			Time++;
			u.Start=Time;
			
			var stack=new Stack<Vertex>();
			stack.Push(u);
			while(stack.Count!=0)
			{
				u=stack.Pop();
				foreach( var i in u.List)
				{
					var v = (Vertex) g[i];
					if(v.Color== Color.White)
					{
						v.Color=Color.Gray;
						Time++;
						v.Start=Time;
						v.Parent=u;
						stack.Push(v);
					}
				}
				u.Color=Color.Black;
				Time++;
				u.Finish=Time;
			}
		}
	}
}



// Define other methods and classes here
