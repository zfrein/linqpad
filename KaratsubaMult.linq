<Query Kind="Program" />

void Main()
{
	var s1="123456789012345678901234567890123456789012345678901234567891";
	var s2="123456789012345678901234567890123456789012345678901234567891";

//Ext.Minus(s1,s2).Dump();


KaratsubaMultiplication(s1,s2).Dump();
	
	
}


string KaratsubaMultiplication(string mult1, string mult2)
{
	
	
	if(mult1.TrimStart('0').Length==0 || mult2.TrimStart('0').Length==0)
		return "0";
	else if (mult1.TrimStart('0').Length==1 && mult2.TrimStart('0').Length==1)
		return (Int32.Parse(mult1)*Int32.Parse(mult2)).ToString();

	var n = Ext.FixStringLength(ref mult1,  ref mult2);
 
	
	var a = mult1.Substring(0,n/2);
	var b = mult1.Substring(n/2);
	
	var c = mult2.Substring(0,n/2);
	var d = mult2.Substring(n/2);
	
	var ac=KaratsubaMultiplication(a,c);
	var bd=KaratsubaMultiplication(b,d);
	var apb=a.Plus(b);
	var cpd=c.Plus(d);
	var apbmcpd=KaratsubaMultiplication(apb, cpd);
	var adbc=apbmcpd.Minus(ac).Minus(bd);
	
	
	
	return (ac+string.Join("",Enumerable.Repeat("0", n))).Plus
	(adbc+string.Join("",Enumerable.Repeat("0", n/2))).Plus(bd);

}

static class Ext
{
	public static string Plus(this string s1, string s2)
	{
		var n = Ext.FixStringLength(ref s1,  ref s2);	
		var a = s1.ToCharArray().Select(x=>Int32.Parse(x.ToString())).ToArray();
		var b = s2.ToCharArray().Select(x=>Int32.Parse(x.ToString())).ToArray();
		
		var res = new int[n+1];
		
		int um = 0;
		for(var i = n - 1; i >= 0; i--)
		{
			res[i+1] = (a[i] + b[i] + um) % 10;
			um = (a[i] + b[i] + um) / 10;
		}
		res[0]=um;
		
		return string.Join("", res).TrimStart('0');
	}
	
	public static string  Minus(this string s1, string s2)
	{
		var n = Ext.FixStringLength(ref s1,  ref s2);	
		var a = s1.ToCharArray().Select(x=>Int32.Parse(x.ToString())).ToArray();
		var b = s2.ToCharArray().Select(x=>Int32.Parse(x.ToString())).ToArray();
		
		var res = new int[n];
		int um = 0;
		for(var i = n - 1; i >= 0; i--)
		{
			if(a[i] >= b[i]+um)
			{
				res[i] = a[i] - b[i] - um;
				um=0;
			}
			else
			{
				res[i] = 10 + a[i] - b[i] - um;
				um=1;
			}
			
		}
		return string.Join("", res).TrimStart('0');
	}
	

	public static int FixStringLength(ref string s1, ref string s2)
	{
		s1=s1.TrimStart('0');
		s2=s2.TrimStart('0');
	
		if(s1.Length >= s2.Length)
		{
			s1 = s1.Length % 2 == 1? "0" + s1 : s1;
			s2 = string.Join("",Enumerable.Repeat("0", s1.Length-s2.Length))+s2;
			
		}
		else if(s1.Length < s2.Length)
		{
			s2 = s2.Length % 2 == 1? "0" + s2 : s2;
			s1 = string.Join("",Enumerable.Repeat("0", s2.Length-s1.Length))+s1;
		}
		
		return s1.Length;
	}


}



