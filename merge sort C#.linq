<Query Kind="Program" />

void Main()
{
    var src = Enumerable.Range(0, 10000000).Reverse().ToArray();
    MergeSort(src);
}

int[] MergeSort(int[] input)
{
	var n = input.Length;

	if (n == 1)
	{
		return new[] { input[0] };
	}
    
    var left = Slice(input, 0, n / 2);
    var right = Slice(input, n / 2);

	var sortedLeft = MergeSort(left);
	var sortedRight = MergeSort(right);

    var result = new int[n];
	for (int k = 0, i = 0, j = 0; k < n; k++)
	{
		if (i < sortedLeft.Length && (j >= sortedRight.Length || sortedLeft[i] < sortedRight[j]))
		{
            result[k] = sortedLeft[i];
			i++;
		}
		else
		{
            result[k] = sortedRight[j];
            j++;
		}
	}
    return result;
}

int[] Slice(int[] arr, int start, int length)
{
	if (start < 0 || start >= arr.Length)
		throw new ArgumentException("start must be within array border");

	if (length < 0)
		throw new ArgumentException("length must be positive number");

	if (length + start > arr.Length)
		throw new ArgumentException("length + start must be in array border");

	var result = new int[length];
	for (int i = 0; i < length; i++)
	{
		result[i] = arr[i + start];
	}

	return result;
}

int[] Slice(int[] arr, int start)
{
    return Slice(arr, start, arr.Length - start);
}