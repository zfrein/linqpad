<Query Kind="Program" />

void Main()
{
	GetRandomiDistinctArray(10, 5).Dump();
}


public int[] GetRandomiDistinctArray(int max, int num)
{
	var res = Enumerable.Range(0, max).Dump();
	var random = new Random();
	return res.OrderBy(x => random.Next()).Take(num).ToArray();
}

// Define other methods and classes here
