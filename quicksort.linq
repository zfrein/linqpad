<Query Kind="Program" />

void Main()
{
	//var a = new int[]{ 1 ,3,5,4,7,8,9,2,6};
	
	//var a=GenerateIntSet(10);
	
	var a = File.ReadAllLines(@"C:\Users\sovchinnikov\Documents\LINQPad Queries\kargerMinCut.txt").Select(Int32.Parse).ToArray();
	

	
//	a.Dump();
	//a.Dump();
//	GetPivotIndex(a, PivotType.Last,0,a.Count()-1).Dump();
	
	//Swap(ref a[0], ref a[1]);
	
	QuickSort(a, PivotType.Middle, 0, a.Count()-1).Dump();
	
	a.Dump();
}


public int QuickSort(int[] arr, PivotType pt, int left, int right)
{
	if(left>=right)
		return 0;

	var comp= right - left;

	var pivot=GetPivotIndex(arr, pt,left, right);
	
	//arr[pivot].Dump("Pivot");
	
	
	Swap(ref arr[left], ref arr[pivot]);
	
	//arr.Dump();
	
	var i = left + 1;
	for (var j = left + 1; j <= right; j++)
	{
		if (arr[j] < arr[left])
		{
			Swap(ref arr[i], ref arr[j]);
			i++;
		}
		//arr.Dump();
	}
	
	Swap(ref arr[left], ref arr[i-1]);
	
	//arr.Dump();
	
	comp += QuickSort(arr,pt,left,i-2);
	comp += QuickSort(arr, pt,i,right);
	
	return comp;
}

public void Swap(ref int a, ref int b)
{
	var temp=a;
	a=b;
	b=temp;
}

public int GetPivotIndex(int[] arr, PivotType pt, int left, int right)
{
	if( pt == PivotType.First )
		return left;
	else if( pt == PivotType.Last )
		return right;
	else if( pt == PivotType.Middle )
	{
		var f=arr[left];
		var l=arr[right];
		var m=arr[(right+left)/2];
		if(m.IsBetween(f,l))
			return (right+left)/2;
		else if(f.IsBetween(m,l))
			return left;
		else if (l.IsBetween(m,f))
			return right;
	}
	throw new Exception();
		
}

public enum PivotType{
First=1,
Last=2,
Middle=3
}
int[] GenerateIntSet(int count)
{
    var rand = new Random();
    var result = new int[count];
    for (int i = 0; i < count; i++)
        result[i] = rand.Next();
    
    return result;
}