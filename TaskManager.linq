<Query Kind="Program">
  <Namespace>System.Collections.Concurrent</Namespace>
</Query>

//необходимо, что бы очередь задач выполнялась в заданном количестве потоков.

void Main()
{
	var tm=new TaskManager(3);
	
	//tm.Add(new Action(Console.WriteLine("aaa0"));
	tm.Add((Action)(()=>1.Dump()));
	Thread.Sleep(100);
	tm.Add((Action)(()=>2.Dump()));
	
	tm.Add((Action)(()=>3.Dump()));
	
	Thread.Sleep(100);
	tm.Add((Action)(()=>4.Dump()));
	
	
	tm.Add((Action)(()=>5.Dump()));
	
	
	
	
	
	Thread.Sleep(100);
	Console.ReadLine();
	
}

public interface ITaskManager
{
	void Add(Action action);
}

public class TaskManager:ITaskManager
{
	private Object obj = new Object();

	private readonly Thread[] threads;

	public TaskManager(int threadCount)
	{
		threads=new Thread[threadCount];
		
		for (int i = 0; i < threadCount; i++)
      		(threads [i] = new Thread (Run)).Start();
		
	}
	private Queue<Action> QueueActions=new Queue<Action>();
	
	public void Add(Action action)
	{	
		if(action!=null)
			Enqueue(action);
		else 
			throw new NullReferenceException();
	}
	
	private void Enqueue(Action action)
	{
		lock(obj)
		{
			QueueActions.Enqueue(action);
			Monitor.Pulse(obj);
		}
	}
	
	private void Run()
	{
		while (true)                        
    	{   
			Action action;
			lock (obj)
			{
				if (QueueActions.Count == 0) 
				 	Monitor.Wait (obj);
        		action = QueueActions.Dequeue();
			}
			if (action == null) return;         
      			action();  
    	}
	}
	
	
	public void Stop ()
  {
    
    foreach (Thread thread in threads)
      	Enqueue(null);
 
     foreach (Thread thread in threads)
        thread.Join();
  }
	
}