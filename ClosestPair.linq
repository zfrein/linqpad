<Query Kind="Program" />

void Main()
{
	var n=50;

	var a = GeneratePointSet(n).DistinctBy(p => new { p.X }).ToArray();
	a.Dump("A");
	var b = MergeSort(a, Coordinates.x);
	//b.Dump("X");
	var c =MergeSort(a, Coordinates.y);
	//c.Dump("Y");
	
	ClosestPairBrutForce(a).Dump();
	
	ClosestPoints(b,c).Dump();
	
	
}

Tuple<Point, Point> ClosestPoints(Point[] Px, Point[] Py)
{
	var n=Px.Length;

	if(n==2)
		return Tuple.Create<Point, Point>(Px[0],Px[1]);
	
	if(n==3)
		return MinDist3(Px[0],Px[1],Px[2]);

	var qLen=n/2;
	var rLen=n-n/2;
		
	var xs=Px[qLen-1].X;	
		
	var Qx=new Point[qLen];
	var Qy=new Point[qLen];
	
	var Rx=new Point[rLen];
	var Ry=new Point[rLen];
	
	for(var i = 0; i < qLen; i++)
	{
		Qx[i]=Px[i];
	}
	for(var i = 0; i < rLen; i++)
	{
		Rx[i] = Px[i + qLen];
	}
	var k=0;
	var j=0;
	for(var i = 0; i < n; i++)
	{
		if(Py[i].X <= xs)
		{
			Qy[k] = Py[i];
			k++;
		}
		else
		{
			Ry[j] = Py[i];
			j++;
		}
	}
	
	var p1=ClosestPoints(Qx,Qy);
	var p2=ClosestPoints(Rx,Ry);
	
	var d1=Distance(p1.Item1,p1.Item2);
	var d2=Distance(p2.Item1,p2.Item2);
	
	var h = Math.Min(d1, d2);
	
	var p3 = ClosestPointsSplit(Py,h,xs);
	
	if(p3 != null)
	{
		var d3 = Distance(p3.Item1, p3.Item2);
		
		if(d1<d2 && d1<d3)
			return p1;
		else if(d2<d1 && d2<d3)
			return p2;
		else if (d3<d1 && d3<d2)
			return p3;
		else
		{
			throw new Exception();
		}
	}
	else
	{
		if(d1<d2)
			return p1;
		else
			return p2;
	}
}

Tuple<Point, Point> ClosestPointsSplit(Point[] Py, double h, double xs)
{
	var Sy=new List<Point>();
	
	for (var i=0; i < Py.Length; i++)
	{
		if(Py[i].X < xs+h && Py[i].X > xs - h)
			Sy.Add(Py[i]);
	}
		var modS=Sy.Count()>=7 ? 7 :Sy.Count();
		
		var  best = h;
		Tuple<Point,Point> bestPair=null;
		for(var i = 0; i<(Sy.Count()>=7 ? Sy.Count() - 7   : 1); i++)
			for(var j = 1; j < Sy.Count(); j++)
			{
				var d=Distance(Sy[i], Sy[i+j]);
				if(d < best )				
				{
					best = d;
					bestPair=Tuple.Create(Sy[i],Sy[i+j]);
					
				}
			}
	
	
	return bestPair;
}

Tuple<Point, Point> ClosestPairBrutForce(Point[] P)
{
    var minD = double.MaxValue;
    var x = default(Point);
    var y = default(Point);
    for (int i = 0; i < P.Length; i++)
    {
        for (int j = i+1; j < P.Length; j++)
        {
            var d = Distance(P[i], P[j]);
            if (d < minD)
            {
                minD = d;
                x = P[i];
                y = P[j];
            }
        }
    }
    return Tuple.Create(x, y);
}


Tuple<Point, Point> MinDist3(Point p1, Point p2, Point p3)
{
    var d1 = Distance(p1, p2);
    var d2 = Distance(p2, p3);
    var d3 = Distance(p1, p3);

    if (d1 <= d2 && d1 <= d3)
        return Tuple.Create(p1, p2);

    if (d2 <= d1 && d2 <= d3)
        return Tuple.Create(p2, p3);

    return Tuple.Create(p1, p3);
}

double Distance(Point p, Point q)
{
    return Math.Sqrt((p.X - q.X) * (p.X - q.X) + (p.Y - q.Y) * (p.Y - q.Y));
}


Point[] MergeSort(Point[] input, Coordinates c)
{
		var n = input.Length;
	
		if (n == 1)
		{
			return new[] { input[0] };
		}
	    
	    var left = Slice(input, 0, n / 2);
	    var right = Slice(input, n / 2);
	
		var sortedLeft = MergeSort(left, c);
		var sortedRight = MergeSort(right, c);
	
	    var result = new Point[n];
		for (int k = 0, i = 0, j = 0; k < n; k++)
		{
			if (i < sortedLeft.Length && (j >= sortedRight.Length 
			|| (c==Coordinates.x ? sortedLeft[i].X : sortedLeft[i].Y)< (c == Coordinates.x ?sortedRight[j].X : sortedRight[j].Y)))
			{
	            result[k] = sortedLeft[i];
				i++;
			}
			else
			{
	            result[k] = sortedRight[j];
	            j++;
			}
		}
	    return result;
}

Point[] Slice(Point[] arr, int start, int length)
{
	if (start < 0 || start >= arr.Length)
		throw new ArgumentException("start must be within array border");

	if (length < 0)
		throw new ArgumentException("length must be positive number");

	if (length + start > arr.Length)
		throw new ArgumentException("length + start must be in array border");

	var result = new Point[length];
	for (int i = 0; i < length; i++)
	{
		result[i] = arr[i + start];
	}

	return result;
}

Point[] Slice(Point[] arr, int start)
{
    return Slice(arr, start, arr.Length - start);
}

struct Point
{
    public Point(double x, double y):this()
    {
        X = x;
        Y = y;
    }

    public double X { get; private set; }
    public double Y { get; private set; }
}



Point[] GeneratePointSet(int count)
{
    var rand = new Random();
    var result = new Point[count];
    for (int i = 0; i < count; i++)
        result[i] = new Point(rand.NextDouble(), rand.NextDouble());
    
    return result;
}


int[] GetRandomDistinctArray(int max){
  Random r = new Random();
            int[] x = new int[max];
 
            for (int i = 0; i < max; i++)
            {
                bool contains;
                int next;
                do
                {
                    next = r.Next(max+10);
                    contains = false;
                    for (int index = 0; index < i; index++)
                    {
                        int n = x[index];
                        if (n == next)
                        {
                            contains = true;
                            break;
                        }
                    }
                } while (contains);
                x[i] = next;
 
                
            }
		return x;
}




enum Coordinates{
x=1,
y=2
}




