<Query Kind="Program" />


//1.  Write a listbox-style binary search for an ordered array of integers.  
//Listbox-style means that you should return the index of the first item greater than or equal to the item being searched for; 
//if all items are less, you should return the index of the last item.  You are guaranteed that there is at least one item in the array.


void Main()
{
	var a=new []{1};
	a.BinarySearchGreaterThan(0).Dump();
}
public static class A{

public static  int BinarySearchGreaterThan( this int[] array, int value)
{
    int minIndex = 0;
	int maxIndex = array.Length - 1;
	
	if(array[minIndex]<=value)
		return minIndex;
		
	if(array[maxIndex]>=value)
		return maxIndex;
	
	
	while (minIndex <= maxIndex )
	{
	   	int midIndex = (minIndex + maxIndex) / 2;
		
		if(midIndex == 0)
			return 0;
			
		if (value < array[midIndex])
		{
			if (value > array[midIndex - 1])
			{
				return midIndex;
			}
			maxIndex = midIndex - 1;
		}
		else if (value > array[midIndex])
		{
			minIndex = midIndex + 1;
		}
		else
		{
			return midIndex;
		}
	}
	return array.Length - 1;
}
}
// Define other methods and classes here
