<Query Kind="Program">
  <Connection>
    <ID>eeb4b597-14a5-4ca0-a93f-0ced3e9a49ca</ID>
    <Persist>true</Persist>
    <Driver>EntityFramework</Driver>
    <Server>mosquito</Server>
    <CustomAssemblyPath>D:\Source\InternetBank\Releases\Current\Source\InternetBank\IB.Core\bin\Debug\IB.Core.dll</CustomAssemblyPath>
    <CustomTypeName>IB.Core.EntityFramework.IbEntities</CustomTypeName>
    <CustomMetadataPath>res://IB.Core/EntityFramework.IbModel.csdl|res://IB.Core/EntityFramework.IbModel.ssdl|res://IB.Core/EntityFramework.IbModel.msl</CustomMetadataPath>
    <Database>IB_DB</Database>
  </Connection>
</Query>

void Main()
{
var r=new Random();
	int min =100;
	
	var a = File.ReadAllLines(@"C:\Users\sovchinnikov\Documents\LINQPad Queries\kargerMinCut.txt").ToArray();
	
	for(var i=0; i<10000; i++ )
	{
		var list=new List<List<int>>();
		
		
		
		foreach(string c in a)
		{
			var d= Regex.Split(c, @"\D+").Where(s => !string.IsNullOrWhiteSpace(s)).Select(Int32.Parse).ToList();
   			list.Add(d);	
		}
		
		//list.Dump();
		
//		a.Add(new List<int>{1,2,2,3});
//		a.Add(new List<int>{2,1,1,3,3,4,4});
//		a.Add(new List<int>{3,1,2,2,4});
//		a.Add(new List<int>{4,2,3,2});
//------------------------------------------------------
//		a.Add(new List<int>{1,2,3,4});
//		a.Add(new List<int>{2,1,3,4,5});
//		a.Add(new List<int>{3,1,2,4});
//		a.Add(new List<int>{4,1,2,3,7});
//		a.Add(new List<int>{5,2,6,7,8});
//		a.Add(new List<int>{6,5,7,8});
//		a.Add(new List<int>{7,4,5,6,8});
//		a.Add(new List<int>{8,5,6,7});
//		
	
		var b=RandomCutCount(list,r);
		if(b<min)
			min=b;
		
		
	}
	min.Dump();
}

int RandomCutCount(List<List<int>> graf, Random r)
{
	int count=0;

	//graf.Dump();
	
	
	
	while(graf.Count>2)
	{
		var edges=new List<Tuple<int,int>>();
	
		foreach(var g in graf)
		{
			for(int i=1; i<g.Count; i++)
			{
				if(g[0] < g[i])
					edges.Add(new Tuple<int,int>(g[0],g[i]));
			}
		}
		
		//edges.Dump();

		var remEdgeIndex = r.Next(edges.Count-1);
		var remEdge=edges[remEdgeIndex];
		//remEdge.Dump();
		edges.Remove(remEdge);
		
				
		//удаляем ребро из списка
		var ver1 = graf.FirstOrDefault(x => x[0] == remEdge.Item1);
		var ver12 = ver1.Where(x => x == remEdge.Item2).ToList();
		foreach(var v in ver12)
		{
			ver1.Remove(v);
		}
			
		var ver2 = graf.FirstOrDefault(x => x[0] == remEdge.Item2);
		var ver22 = ver2.Where(x => x == remEdge.Item1).ToList();
		foreach(var v in ver22)
		{
			ver2.Remove(v);
		}
		//ver2.Remove(ver2.FirstOrDefault(x=>x==remEdge.Item1));
		
		//мержим - соединяем вершины
		//graf.Dump();
		
		for(var i=1; i< ver2.Count; i++)
		{
			var v = graf.FirstOrDefault(x=>x[0]==ver2[i]);
			var a = v.FirstOrDefault(x=>x==ver2[0]);
			if(a!=null)
			{	v.Remove(a);
				v.Add(ver1[0]);
			}	
		}
		
		//graf.Dump();
		
		//добавляем смежные вершины2 к первой
		for(var i=1; i< ver2.Count; i++)
		{
			ver1.Add(ver2[i]);
		}
		graf.Remove(ver2);
		
		
		//graf.Dump();
		count++;
	}
	
	return graf[0].Count-1;

}

