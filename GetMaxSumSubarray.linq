<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Numerics.dll</Reference>
  <Namespace>System.Numerics</Namespace>
</Query>

void Main()
{
		var a=new []{1,-1,4,-3,4,-5};
	GetMaxSumSubarray(a).Dump();
}



//2.Suppose you have an array of integers, both positive and negative, in no particular order.  
//Find the largest possible sum of any continuous subarray.  
//For example, if you have all positive numbers, the largest sum would be the sum of the whole array; if you have all negative numbers, the largest sum is 0 (the null subarray)
public BigInteger GetMaxSumSubarray(int[] array)
{
	BigInteger tempMaximumSum = 0;
	BigInteger maximumSum = 0;
	
	foreach (var item in array)
	{
		tempMaximumSum += item;
		if (tempMaximumSum < 0)
		tempMaximumSum = 0;
		
		if (tempMaximumSum > maximumSum)
		maximumSum = tempMaximumSum;
	}
	
	return maximumSum;
}

// Define other methods and classes here
