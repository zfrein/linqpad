<Query Kind="Program" />

void Main()
        {
            var q = new PriorityQueue<object>();
			var a1=new object();
			
            q.Enqueue( a1,1);
            q.Enqueue("2",2);
            q.Enqueue("3",3);
            q.Enqueue("4",4);
            q.Enqueue("5",5);
            q.Enqueue(a1,1);
            q.Enqueue("7",1);
			q.Position("7").Dump();

//            q.Dequeue();

        }

        interface IQueue<T>
        {
            void Enqueue(T value, int priority); //Добавление элемента в очередь с заданым приоритетом
            T Pop(); //Извлечение первого элемента из очереди
            int Length(); //Получение длины очереди
            int Position(T value); //Получение позиции элемента в очереди с учётом приоритета
        }

        public class PriorityQueue<T> : IQueue<T>
        {
            private List<KeyValuePair<int, T>> baseHeap;

            public PriorityQueue()
            {
                baseHeap = new List<KeyValuePair<int, T>>();
            }

            public void Enqueue(T value, int priority)
            {
                KeyValuePair<int, T> val = new KeyValuePair<int, T>(priority, value);
                baseHeap.Add(val);

                HeapifyFromEndToBeginning(baseHeap.Count() - 1);
            }

            public T Pop()
            {
                return Dequeue().Value;
            }

            private int HeapifyFromEndToBeginning(int pos)
            {
                if (pos >= baseHeap.Count) return -1;

                // heap[i] have children heap[2*i + 1] and heap[2*i + 2] and parent heap[(i-1)/ 2];

                while (pos > 0)
                {
                    int parentPos = (pos - 1) / 2;
                    if (baseHeap[parentPos].Key > baseHeap[pos].Key)
                    {
                        this.SwapElements(parentPos, pos);
                        pos = parentPos;
                    }
                    else break;

                }
                return pos;
            }





            public KeyValuePair<int, T> Dequeue()
            {
                if (baseHeap.Count != 0)
                {
                    KeyValuePair<int, T> result = baseHeap[0];
                    DeleteRoot();
                    return result;
                }
                else
                    throw new InvalidOperationException("Priority queue is empty");
            }

            private void SwapElements(int pos1, int pos2)
            {
                var val = baseHeap[pos1];
                baseHeap[pos1] = baseHeap[pos2];
                baseHeap[pos2] = val;
            }

            private void DeleteRoot()
            {
                if (baseHeap.Count <= 1)
                {
                    baseHeap.Clear();
                    return;
                }

                baseHeap[0] = baseHeap[baseHeap.Count - 1];
                baseHeap.RemoveAt(baseHeap.Count - 1);

                // heapify
                HeapifyFromBeginningToEnd();
            }

            private void HeapifyFromBeginningToEnd()
            {
                var pos = 0;
                int smallest = 0;

                do
                {
                    pos = smallest;                   

                    // on each iteration exchange element with its smallest child
                    var left = 2 * pos + 1;
                    var right = 2 * pos + 2;
                    if (left < baseHeap.Count &&
                        baseHeap[smallest].Key > baseHeap[left].Key)
                        smallest = left;
                    if (right < baseHeap.Count && baseHeap[smallest].Key > baseHeap[right].Key)
                        smallest = right;

                    this.SwapElements(smallest, pos);
                    
                } while (smallest != pos);
            }


public int Length()
            {
                return baseHeap.Count;
            }

           public int Position(T value)
            {
                var pos = 1;

                var key = baseHeap.First(x => EqualityComparer<T>.Default.Equals(x.Value,value)).Key;

                foreach (var keyValuePair in baseHeap)
                {
                    if (EqualityComparer<T>.Default.Equals(keyValuePair.Value, value))
                        break;

                    if (keyValuePair.Key <= key)
                        pos++;



                }
                return pos;
            }
        }

        // Define other methods and classes here